package org.contactbook;

import org.contactbook.controller.UserController;
import org.contactbook.entity.UserInfo;
import org.contactbook.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.runner.WebApplicationContextRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.ArrayList;
import java.util.List;


import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.*;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestContext.class, WebApplicationContextRunner.class})
@WebAppConfiguration
class MainStarterTests {

	@InjectMocks
	UserController userController;

	@Mock
	UserRepository repository;


	@Test
	public void testAddUser() {
		MockHttpServletRequest request = new MockHttpServletRequest();
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

		UserInfo user = new UserInfo("John Smith", "jsmith@email.com", "447659233645");
		ResponseEntity<Object> responseEntity = userController.addUser(user);

		assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
	}

	@Test
	public void testFindAll() {

		UserInfo user1 = new UserInfo("John Smith", "jsmith@email.com", "07831060134");
		UserInfo user2 = new UserInfo("Jane Rawlings", "jrawlings@email.com", "07651985326");
		List<UserInfo> users = new ArrayList<>();
		users.add(user1);
		users.add(user2);

		when(repository.findAll()).thenReturn(users);

		assertThat(repository.findAll().size()).isEqualTo(2);
		assertThat(repository.findAll().get(0).getName()).isEqualTo(user1.getName());
		assertThat(repository.findAll().get(0).getId()).isEqualTo(user1.getId());
	}

}
