package org.contactbook.repository;

import org.contactbook.entity.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<UserInfo, Long> {

    List<UserInfo> findAll();
    List<UserInfo> findByName(String name);

}
