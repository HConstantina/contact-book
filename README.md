Instructions on how to run the project: 

1. Open the program in your preffered IDE (Java IDE)
2. Run the program from the MainStarter.java class
3. Open up Postman
4. In the 'Headers' section in Postman type in the key column "Content-Type" and in the value column "application/json"
5. Acces one of the desired links used in the UserController.java class: 
        - for instance, if you want to retrieve all users access "localhost:8080/api/users"
